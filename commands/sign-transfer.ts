import { readFileSync } from "fs";
import EthereumTx from "ethereumjs-tx";
import axios from "axios";
import BigNumber from "bignumber.js";

interface Fields {
  id: string;
  type: string;
  from_address: string;
  to_address: string;
  amount: string;
}

interface Response {
  id: string;
  tx: string;
}

function validateFields(fields: Fields): boolean {
  return (
    typeof fields.id === "string" &&
    typeof fields.type === "string" &&
    typeof fields.from_address === "string" &&
    typeof fields.to_address === "string" &&
    typeof fields.amount === "string"
  );
}

function loadPrivateKey(address: string): Buffer {
  const privateKey = readFileSync(address, "utf8");

  return Buffer.from(privateKey, "hex");
}

function retrieveGasPrice(network: string = "ropsten"): Promise<number> {
  return axios({
    method: "get",
    url: `https://api.infura.io/v1/jsonrpc/${network}/eth_gasPrice`
  }).then(response => {
    return parseInt(response.data.result || "0x0", 16);
  });
}

function retrieveNextNonce(address: string, network: string = "ropsten"): Promise<string> {
  return axios({
    method: "get",
    url: `https://api.infura.io/v1/jsonrpc/${network}/eth_getTransactionCount`,
    params: {
      params: JSON.stringify([address, "latest"])
    }
  }).then(response => {
    const nonce = parseInt(response.data.result || "0x0", 16);

    return "0x" + nonce.toString(16);
  });
}

function toHex(amount: number): string {
  return "0x" + amount.toString(16);
}

export default function SignTransfer(fields: Fields): Promise<Response> {
  return new Promise<Response>((resolve, reject) => {
    if (validateFields(fields)) {
      let gasPrice = 0;
      let gasLimit = 21000;
      let value = new BigNumber(0);

      retrieveGasPrice()
        .then(retrievedGasPrice => {
          gasPrice = retrievedGasPrice;

          const amount = parseFloat(fields.amount) || 0;
          const amountInWei = new BigNumber(amount).multipliedBy("1e+18");
          const gasCost = new BigNumber(gasLimit).multipliedBy(gasPrice);

          if (gasCost.isGreaterThan(amountInWei)) throw "Insufficient amount to cover gas cost";

          value = amountInWei.minus(gasCost);

          return retrieveNextNonce(fields.from_address);
        })
        .then(nonce => {
          const transferParameters = {
            nonce: nonce,
            gasPrice: toHex(gasPrice),
            gasLimit: toHex(gasLimit),
            to: fields.to_address,
            value: "0x" + value.toString(16),
            chainId: 3 // ropsten testnet
          };

          const privateKey = loadPrivateKey(fields.from_address);
          const transfer = new EthereumTx(transferParameters);
          transfer.sign(privateKey);

          resolve({ id: fields.id, tx: transfer.serialize().toString("hex") });
        })
        .catch(error => reject(error));
    } else {
      reject("Invalid fields type");
    }
  });
}
