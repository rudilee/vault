export default function Json(json: string) {
  try {
    return JSON.parse(json);
  } catch (error) {
    console.log(error);

    return {};
  }
}
