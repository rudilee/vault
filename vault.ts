import Server from "./server";
import { stat, unlink } from "fs";

let server: Server;

if (process.argv.length == 3) {
  const socketFile = process.argv[2];

  // Check for existing socket file, remove it before creating server
  stat(socketFile, (error, stats) => {
    if (error) {
      server = new Server(socketFile);
    } else {
      unlink(socketFile, error => {
        if (error) process.exit(0);

        server = new Server(socketFile);
      });
    }
  });

  // Close all on user interuption/Ctrl+C
  process.on("SIGINT", () => {
    if (server) {
      server.close();
      server.closeAllConnections();

      process.exit();
    }
  });
}
