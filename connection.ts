import { EventEmitter } from "events";
import { Socket } from "net";

import Codec from "./codec";
import SignTransfer from "./commands/sign-transfer";

enum Commands {
  SignTransfer = "sign_transfer"
}

declare interface Connection {
  on(event: "close", listener: (client: Connection) => void): this;
}

class Connection extends EventEmitter {
  private commands: Map<string, (fields: any) => Promise<any>> = new Map();
  private socket: Socket;

  constructor(socket: Socket) {
    super();

    this.socket = socket;
    this.socket.pipe(new Codec()).on("data", this.handleData.bind(this));
    this.socket.on("close", this.handleClose.bind(this));
    this.socket.on("error", this.handleError.bind(this));

    this.commands.set(Commands.SignTransfer, SignTransfer);
  }

  public close() {
    this.socket.end();
  }

  private handleData(fields: any) {
    if ("type" in fields) {
      if (this.commands.has(fields.type)) {
        const command = this.commands.get(fields.type);

        if (command)
          command
            .call(this, fields)
            .then((response: any) => {
              this.socket.write(Codec.encode(response));
            })
            .catch((error: Error) => console.error(error));
      }
    }
  }

  private handleClose(hadError: boolean) {
    this.emit("close", this);
  }

  private handleError(error: Error) {
    this.socket.end();

    console.error(error);
  }
}

export { Connection };
export default Connection;
