import { Server as SocketServer, Socket } from "net";

import Connection from "./connection";

export default class Server extends SocketServer {
  private connectionList: Connection[] = [];

  constructor(socketFile: string) {
    super();

    this.on("connection", this.handleNewConnection);
    this.listen(socketFile);
  }

  public closeAllConnections() {
    this.connectionList.forEach(connection => connection.close());
  }

  private handleNewConnection(socket: Socket) {
    let connection = new Connection(socket);
    connection.on("close", this.handleConnectionClose.bind(this));

    this.connectionList.push(connection);
  }

  private handleConnectionClose(connection: Connection) {
    let index = this.connectionList.indexOf(connection, 0);
    if (index > -1) {
      this.connectionList.splice(index, 1);
    }
  }
}
