import { Transform } from "stream";

import Json from "./safe-json-parse";

const delimiter: Buffer = Buffer.from("\n");

export default class Codec extends Transform {
  private buffer: Buffer = Buffer.from("");

  constructor() {
    super({ objectMode: true });
  }

  public static encode(response: any): string {
    return JSON.stringify(response) + "\n";
  }

  public _transform(chunk: Buffer, encoding: string, callback: () => void) {
    let splitIndex = 0;

    if (this.buffer.length > 0) chunk = Buffer.concat([this.buffer, chunk]);

    chunk.forEach((character, index) => {
      if (character === delimiter[0]) {
        let line = chunk.slice(splitIndex, index);
        const fields = Json(line.toString(encoding));
        splitIndex = index + 1;

        if (Object.keys(fields).length > 0) this.push(fields);
      }
    });

    if (splitIndex !== chunk.length)
      this.buffer = Buffer.concat([this.buffer, chunk.slice(splitIndex)]);

    callback();
  }
}
